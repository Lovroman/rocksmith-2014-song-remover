﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace _7ZipTestRocksmith.Utilities
{
    public static class Extensions
    {
        public static string TuningToName(this string tuningStrings)
        {
            var root = XElement.Load("tunings.xml");
            var tuningName = root.Elements("Tuning").Where(tuning => tuning.Attribute("Strings").Value == tuningStrings).Select(tuning => tuning.Attribute("Name")).ToList();
            return tuningName.Count == 0 ? "Other" : tuningName[0].Value;
        }
        public static void InvokeIfRequired<T>(this T c, Action<T> action) where T : Control
        {
            if (c.InvokeRequired)
                c.Invoke(new Action(() => action(c)));
            else
                action(c);
        }
    }
}
