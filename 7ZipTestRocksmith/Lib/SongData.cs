﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _7ZipTestRocksmith
{
    class SongData
    {
        public string Name { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Tuning { get; set; }
        public string SongKey { get; set; }
        public string Enabled { get; set; }
    }
}
