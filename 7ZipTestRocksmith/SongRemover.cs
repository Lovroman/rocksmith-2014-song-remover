﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RocksmithToolkitLib;
using System.Xml.Linq;
using System.Reflection;
using RocksmithToolkitLib.DLCPackage;
using System.Threading;
using _7ZipTestRocksmith.Utilities;
using System.ComponentModel;

namespace _7ZipTestRocksmith
{
    public partial class SongRemover : Form
    {
        private int fullSongCount = 0;
        private string PZipPath = "7za.exe";
        private bool allSelected = false;
        private Settings mySettings;

        RSJsonDictionary RS2014SongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1DLCSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1SongCollection = new RSJsonDictionary();

        RSJsonDictionary RS2014FullSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1DLCFullSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1FullSongCollection = new RSJsonDictionary();

        RSJsonDictionary RS2014DisabledSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1DLCDisabledSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1DisabledSongCollection = new RSJsonDictionary();

        RSJsonDictionary RS2014FullDisabledSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1DLCFullDisabledSongCollection = new RSJsonDictionary();
        RSJsonDictionary RS1FullDisabledSongCollection = new RSJsonDictionary();

        public string tbRSPathText
        {
            get { return tbRSPath.Text; }
        }

        public SongRemover()
        {
            InitializeComponent();
            comboGameChoice.SelectedIndex = 2;
            mySettings = new Settings();
            LoadSettings();
            Type dgvType = dgvSongs.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgvSongs, true, null);
        }

        public void LoadSettings()
        {
            if (File.Exists("settings.json"))
            {
                string settingsJson = File.ReadAllText("settings.json");
                Settings settings = JsonConvert.DeserializeObject<Settings>(settingsJson);
                if (settings != null)
                {
                    tbRSPath.Text = settings.RSPath;
                    chkRemoveDirs.Checked = settings.DeleteDirs;
                }
            }
            else
            {
                mySettings.DeleteDirs = true;
                mySettings.RSPath = "";
                chkRemoveDirs.Checked = true;
            }
        }

        public void SaveSettings()
        {
            mySettings.RSPath = tbRSPath.Text;
            mySettings.DeleteDirs = chkRemoveDirs.Checked;
            string settingsJson = JsonConvert.SerializeObject(mySettings, Formatting.Indented);

            using (StreamWriter file = new StreamWriter("settings.json"))
            {
                file.Write(settingsJson);
            }
        }

        #region Load songs
        public void ExtractSongFile()
        {
            if (File.Exists(@"manifests\songs\songs.hsan"))
                File.Delete(@"manifests\songs\songs.hsan");

            ProcessStartInfo pZip = new ProcessStartInfo();
            pZip.FileName = PZipPath;
            pZip.Arguments = @"e cache_Pc\cache7.7z manifests\songs\songs.hsan -aoa";

            Process x = Process.Start(pZip);
            x.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            x.WaitForExit();

            FileInfo f = new FileInfo("songs.hsan");
            if (f.Length > 0)
            {
                if (!Directory.Exists(@"manifests\songs"))
                    Directory.CreateDirectory(@"manifests\songs");
                File.Copy("songs.hsan", @"manifests\songs\songs.hsan");
                GetSongsFromSongFile();
            }
            else
                MessageBox.Show("Extracting song cache failed!");
        }

        public void GetSongsFromSongFile()
        {
            string rs2014SongFilePath = @"manifests\songs\songs.hsan";
            string rs1SongFilePath = @"rs1compatibilitydisc_p_Pc\manifests\songs_rs1disc\songs_rs1disc.hsan";
            string rs1DLCSongFilePath = @"rs1compatibilitydlc_p_Pc\manifests\songs_rs1dlc\songs_rs1dlc.hsan";

            if (File.Exists(rs2014SongFilePath))
                PopulateSongList(rs2014SongFilePath, RS2014SongCollection, ref RS2014FullSongCollection, RS2014DisabledSongCollection, ref RS2014FullDisabledSongCollection);

            if (File.Exists(rs1SongFilePath))
                PopulateSongList(rs1SongFilePath, RS1SongCollection, ref RS1FullSongCollection, RS1DisabledSongCollection, ref RS1FullDisabledSongCollection);

            if (File.Exists(rs1DLCSongFilePath))
                PopulateSongList(rs1DLCSongFilePath, RS1DLCSongCollection, ref RS1DLCFullSongCollection, RS1DLCDisabledSongCollection, ref RS1DLCFullDisabledSongCollection);

            fullSongCount = RS2014FullSongCollection.Count + RS1FullSongCollection.Count + RS1DLCFullSongCollection.Count;

            // if(fullSongCount > 0)
            PopulateDataGridView();
        }

        private void PopulateSongList(string songFilePath, RSJsonDictionary songCollection, ref RSJsonDictionary fullSongCollection, RSJsonDictionary disabledSongCollection, ref RSJsonDictionary fullDisabledSongCollection)
        {
            string songFileContent = File.ReadAllText(songFilePath);
            var songsJson = JObject.Parse(songFileContent);

            var songsList = songsJson["Entries"];

            fullSongCollection = JsonConvert.DeserializeObject<RSJsonDictionary>(songsList.ToString());

            if (songsJson.ToString().Contains("DisabledSongs"))
            {
                var disabledSongsList = songsJson["DisabledSongs"];
                fullDisabledSongCollection = JsonConvert.DeserializeObject<RSJsonDictionary>(disabledSongsList.ToString());

                AddSongsToNestedDictionary(fullDisabledSongCollection, disabledSongCollection, fullSongCollection, false, true);
            }

            AddSongsToNestedDictionary(fullSongCollection, songCollection, fullSongCollection, true, true);
        }

        private void AddSongsToNestedDictionary(RSJsonDictionary songsToBeAddedCollection, RSJsonDictionary songCollection, RSJsonDictionary fullSongCollection, bool enabled, bool RS2014 = false)
        {
            foreach (KeyValuePair<string, Dictionary<string, RSSongData>> song in songsToBeAddedCollection.ToList())
            {
                foreach (KeyValuePair<string, RSSongData> songAttributes in song.Value)
                {
                    RSSongData songData = (RSSongData)songAttributes.Value;
                    if (songData.SongName != null)
                    {
                        SongData sngData = AttributesToSongData(songData, enabled);

                        if (!sngData.Name.Contains("$[") && !sngData.Name.Contains("RS2") && !sngData.SongKey.Contains("GE_FE_") && !DictionaryContains(songCollection, songData))
                            songCollection.Add(song.Key, song.Value);

                        if (RS2014)
                        {
                            if (sngData.Name.Contains("$[") || sngData.Name.Contains("RS2") || sngData.SongKey.Contains("GE_FE_"))
                                fullSongCollection.Remove(song.Key);
                        }
                    }
                }
            }
        }

        public void PopulateDataGridView()
        {
            dgvSongs.InvokeIfRequired(delegate
            {
                dgvSongs.Rows.Clear();
            });

            comboGameChoice.InvokeIfRequired(delegate
            {
                if (comboGameChoice.SelectedIndex == 2)
                {
                    AddToDGV(RS2014DisabledSongCollection, false);
                    AddToDGV(RS2014SongCollection, true);
                }
                else if (comboGameChoice.SelectedIndex == 1)
                {
                    AddToDGV(RS1DLCDisabledSongCollection, false);
                    AddToDGV(RS1DLCSongCollection, true);
                }
                else
                {
                    AddToDGV(RS1DisabledSongCollection, false);
                    AddToDGV(RS1SongCollection, true);
                }
            });
        }

        public bool UnpackSongCache()
        {
            if (Directory.Exists(tbRSPath.Text))
            {
                Packer.Unpack(Path.Combine(tbRSPath.Text, "cache.psarc"), "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                if (File.Exists(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydisc_p.psarc")))
                    Packer.Unpack(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydisc_p.psarc"), "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                if (File.Exists(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydlc_p.psarc")))
                    Packer.Unpack(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydlc_p.psarc"), "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                return true;
            }
            else
            {
                if (File.Exists("cache.psarc") || File.Exists("rs1compatibilitydisc_p.psarc") || File.Exists("rs1compatibilitydlc_p.psarc"))
                {
                    Packer.Unpack("cache.psarc", "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                    if (File.Exists("rs1compatibilitydisc_p.psarc"))
                        Packer.Unpack("rs1compatibilitydisc_p.psarc", "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                    if (File.Exists("rs1compatibilitydlc_p.psarc"))
                        Packer.Unpack("rs1compatibilitydlc_p.psarc", "", new Platform(GamePlatform.Pc, GameVersion.RS2014));
                    return true;
                }
                else
                {
                    MessageBox.Show("Please enter your Rocksmith path or copy the app into your Rocksmith folder!");
                    return false;
                }
            }
        }
        #endregion
        #region Save songs
        private void DeleteFoldersAndFiles()
        {
            comboGameChoice.InvokeIfRequired(delegate
            {
                if (comboGameChoice.SelectedIndex == 1)
                {
                    if (Directory.Exists("rs1compatibilitydlc_p_Pc"))
                        Directory.Delete("rs1compatibilitydlc_p_Pc", true);
                }
                else if (comboGameChoice.SelectedIndex == 0)
                {
                    if (Directory.Exists("rs1compatibilitydisc_p_Pc"))
                        Directory.Delete("rs1compatibilitydisc_p_Pc", true);
                }
                else
                {
                    if (Directory.Exists("cache_Pc"))
                        Directory.Delete("cache_Pc", true);
                    if (Directory.Exists("manifests"))
                        Directory.Delete("manifests", true);
                    if (File.Exists("songs.hsan"))
                        File.Delete("songs.hsan");
                }
            });
        }

        private bool RepackSong(string songPath)
        {
            string songExtractedFolder = songPath + "_Pc";
            string songPsarc = songPath + ".psarc";
            string songRSPsarc = Path.Combine(tbRSPath.Text, "dlc", songPsarc);
            string songRSPsarcBackup = Path.Combine(tbRSPath.Text, "dlc", songPsarc) + ".backup";

            if (Directory.Exists(songExtractedFolder))
            {
                if (File.Exists(songPsarc))
                {
                    File.SetAttributes(songPsarc, FileAttributes.Normal);
                    File.Delete(songPsarc);
                }

                Packer.Pack(Path.Combine(Directory.GetCurrentDirectory(), songExtractedFolder), songPath);

                if (Directory.Exists(tbRSPath.Text))
                {
                    if (!File.Exists(songRSPsarcBackup) && File.Exists(songRSPsarc))
                    {
                        if (songRSPsarc.Contains("cache"))
                            File.Copy(songPsarc, Path.Combine(tbRSPath.Text, songPsarc) + ".backup", true);
                        else
                            File.Copy(songRSPsarc, songRSPsarcBackup);
                    }
                    if (songRSPsarc.Contains("cache") && tbRSPath.Text != Directory.GetCurrentDirectory())
                        File.Copy(songPsarc, Path.Combine(tbRSPath.Text, songPsarc), true);
                    else
                        File.Copy(songPsarc, songRSPsarc, true);
                }
                return true;
            }
            else
                return false;
        }

        public void RepackSongCache()
        {
            string unpackedPsarcPath = "cache_Pc";
            try
            {
                comboGameChoice.InvokeIfRequired(delegate
                {
                    if (comboGameChoice.SelectedIndex == 2)
                    {
                        if (Directory.Exists(unpackedPsarcPath))
                        {
                            if (!File.Exists(Path.Combine(unpackedPsarcPath, "sltsv1_aggregategraph.nt")))
                                File.Copy("sltsv1_aggregategraph.nt", Path.Combine(unpackedPsarcPath, "sltsv1_aggregategraph.nt"));

                            RepackSong("cache");
                            MessageBox.Show("RS2014 songs saved!", "Done");
                        }
                    }
                    else if (comboGameChoice.SelectedIndex == 1)
                    {
                        if (RepackSong("rs1compatibilitydlc_p"))
                            MessageBox.Show("RS1 DLC songs saved!", "Done");
                        else
                            MessageBox.Show("Repacking RS1 DLC file failed! ", "Failed");
                    }
                    else
                    {
                        if (RepackSong("rs1compatibilitydisc_p"))
                            MessageBox.Show("RS1 disc songs saved!", "Done");
                        else
                            MessageBox.Show("Repacking RS1 song file failed! ", "Failed");
                    }
                });
            }
            catch (AccessViolationException)
            {
                MessageBox.Show("Please close Rocksmith and then try to save the songs again!");
            }
        }
        private void SerializeSongFile(string songFilePath, RSJsonDictionary fullSongCollection, RSJsonDictionary FullDisabledSongCollection)
        {
            using (StreamWriter file = new StreamWriter(songFilePath))
            {
                dynamic songJson = new
                {
                    Entries = fullSongCollection,
                    InsertRoot = "Static.Songs.Headers",
                    DisabledSongs = FullDisabledSongCollection
                };

                JToken serializedJson = JsonConvert.SerializeObject(songJson, Formatting.Indented, new JsonSerializerSettings { });

                file.Write(serializedJson.ToString());
            }
        }
        public void UpdateHSANSongFile()
        {
            string rs2014SongFilePath = @"manifests\songs\songs.hsan";
            string rs1SongFilePath = @"rs1compatibilitydisc_p_Pc\manifests\songs_rs1disc\songs_rs1disc.hsan";
            string rs1DLCSongFilePath = @"rs1compatibilitydlc_p_Pc\manifests\songs_rs1dlc\songs_rs1dlc.hsan";

            comboGameChoice.InvokeIfRequired(delegate
            {
                if (comboGameChoice.SelectedIndex == 2)
                {
                    if (File.Exists(rs2014SongFilePath))
                    {
                        File.Delete(rs2014SongFilePath);
                    }
                    SerializeSongFile(rs2014SongFilePath, RS2014FullSongCollection, RS2014FullDisabledSongCollection);
                }
                else if (comboGameChoice.SelectedIndex == 1)
                {
                    if (File.Exists(rs1DLCSongFilePath))
                    {
                        SerializeSongFile(rs1DLCSongFilePath, RS1DLCFullSongCollection, RS1DLCFullDisabledSongCollection);
                    }
                }
                else
                {
                    if (File.Exists(rs1SongFilePath))
                    {
                        SerializeSongFile(rs1SongFilePath, RS1FullSongCollection, RS1FullDisabledSongCollection);
                    }
                }
            });
        }
        private void UpdateSongFile()
        {
            comboGameChoice.InvokeIfRequired(delegate
            {
                if (comboGameChoice.SelectedIndex == 2)
                {
                    ProcessStartInfo pZip = new ProcessStartInfo();
                    pZip.FileName = PZipPath;
                    pZip.Arguments = @"u cache_Pc\cache7.7z manifests\songs\songs.hsan";
                    Process x = Process.Start(pZip);
                    x.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    x.WaitForExit();
                }
            });
        }
        #endregion
        #region Enable 'n disable
        private void DisableSong(RSJsonDictionary songCollection, RSJsonDictionary fullSongCollection, RSJsonDictionary disabledSongCollection, RSJsonDictionary fullDisabledSongCollection, string rowSongKey)
        {
            var matchingSongs = fullSongCollection.Where(x => x.Value.Any(song => song.Key == "Attributes" && song.Value.SongKey == rowSongKey)).Select(z => z.Key).ToList();

            if (matchingSongs.Count > 0)
            {
                matchingSongs.ForEach(sngKey =>
                {
                    fullDisabledSongCollection.Add(sngKey, fullSongCollection[sngKey]);
                    fullSongCollection.Remove(sngKey);
                    songCollection.Remove(sngKey);
                });

                disabledSongCollection.Add(matchingSongs[0], fullDisabledSongCollection[matchingSongs[0]]);
            }
        }

        private void EnableSong(RSJsonDictionary songCollection, RSJsonDictionary fullSongCollection, RSJsonDictionary disabledSongCollection, RSJsonDictionary fullDisabledSongCollection, string rowSongKey)
        {
            var matchingSongs = fullDisabledSongCollection.Where(x => x.Value.Any(song => song.Value.SongKey == rowSongKey)).Select(z => z.Key).ToList();

            if (matchingSongs.Count > 0)
            {
                matchingSongs.ForEach(sngKey =>
                {
                    fullSongCollection.Add(sngKey, fullDisabledSongCollection[sngKey]);
                    fullDisabledSongCollection.Remove(sngKey);
                    disabledSongCollection.Remove(sngKey);
                });

                fullSongCollection.OrderBy(sng => sng.Key);
                songCollection.Add(matchingSongs[0], fullSongCollection[matchingSongs[0]]);
            }
        }
        #endregion
        #region Events
        private void btnRestoreBackup_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(tbRSPath.Text))
            {
                if (File.Exists(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydisc_p.psarc.backup")))
                {
                    File.Copy(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydisc_p.psarc.backup"), Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydisc_p.psarc"), true);
                }
                if (File.Exists(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydlc_p.psarc.backup")))
                {
                    File.Copy(Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydlc_p.psarc.backup"), Path.Combine(tbRSPath.Text, "dlc", "rs1compatibilitydlc_p.psarc"), true);
                }
                if (File.Exists(Path.Combine(tbRSPath.Text, "cache.psarc.backup")))
                {
                    File.Copy(Path.Combine(tbRSPath.Text, "cache.psarc.backup"), Path.Combine(tbRSPath.Text, "cache.psarc"), true);
                }
            }
        }

        private void btnZipForm_Click(object sender, EventArgs e)
        {
            ProjectArchiver pa = new ProjectArchiver();
            pa.Show();
        }
        private void comboGameChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            allSelected = false;
            PopulateDataGridView();
        }

        private void SongRemover_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

        private void ClearCollections()
        {
            RS2014FullDisabledSongCollection.Clear();
            RS2014FullSongCollection.Clear();
            RS2014DisabledSongCollection.Clear();
            RS2014SongCollection.Clear();

            RS1FullDisabledSongCollection.Clear();
            RS1FullSongCollection.Clear();
            RS1DisabledSongCollection.Clear();
            RS1SongCollection.Clear();

            RS1DLCFullDisabledSongCollection.Clear();
            RS1DLCFullSongCollection.Clear();
            RS1DLCDisabledSongCollection.Clear();
            RS1DLCSongCollection.Clear();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            ClearCollections();
            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += delegate
            {
                if (UnpackSongCache())
                {
                    ExtractSongFile();
                }
                else
                    MessageBox.Show("Unpacking song cache failed!");
            };
            bgWorker.RunWorkerAsync();
        }

        private void btnSaveSongs_Click(object sender, EventArgs e)
        {
            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += delegate
            {
                UpdateHSANSongFile();
                UpdateSongFile();
                RepackSongCache();
                if (chkRemoveDirs.Checked)
                {
                    DeleteFoldersAndFiles();
                }
            };
            bgWorker.RunWorkerAsync();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvSongs.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["colSelect"];
                chk.Value = !allSelected;
            }
            allSelected = !allSelected;
        }

        private void btnDeleteSong_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvSongs.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["colSelect"];
                if (chk.Value.ToString().ToLower() == "true")
                {
                    string rowSongKey = row.Cells["colSongKey"].Value.ToString();
                    if (comboGameChoice.SelectedIndex == 2)
                    {
                        DisableSong(RS2014SongCollection, RS2014FullSongCollection, RS2014DisabledSongCollection, RS2014FullDisabledSongCollection, rowSongKey);
                    }
                    else if (comboGameChoice.SelectedIndex == 0)
                    {
                        DisableSong(RS1SongCollection, RS1FullSongCollection, RS1DisabledSongCollection, RS1FullDisabledSongCollection, rowSongKey);
                    }
                    else
                    {
                        DisableSong(RS1DLCSongCollection, RS1DLCFullSongCollection, RS1DLCDisabledSongCollection, RS1DLCFullDisabledSongCollection, rowSongKey);
                    }
                    if (row.Index != -1)
                    {
                        row.Cells["colEnabled"].Value = "No";
                    }
                }
            }
        }

        private void btnEnableSong_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvSongs.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells["colSelect"];
                if (chk.Value.ToString().ToLower() == "true")
                {
                    string rowSongKey = row.Cells["colSongKey"].Value.ToString();
                    if (comboGameChoice.SelectedIndex == 2)
                    {
                        EnableSong(RS2014SongCollection, RS2014FullSongCollection, RS2014DisabledSongCollection, RS2014FullDisabledSongCollection, rowSongKey);
                    }
                    else if (comboGameChoice.SelectedIndex == 0)
                    {
                        EnableSong(RS1DLCSongCollection, RS1FullSongCollection, RS1DisabledSongCollection, RS1FullDisabledSongCollection, rowSongKey);
                    }
                    else
                    {
                        EnableSong(RS1DLCSongCollection, RS1DLCFullSongCollection, RS1DLCDisabledSongCollection, RS1DLCFullDisabledSongCollection, rowSongKey);
                    }
                    if (row.Index != -1)
                    {
                        row.Cells["colEnabled"].Value = "Yes";
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboGameChoice.SelectedIndex == 2)
                RefreshDGVAfterSearching(RS2014SongCollection, RS2014DisabledSongCollection);
            else if (comboGameChoice.SelectedIndex == 1)
                RefreshDGVAfterSearching(RS1DLCSongCollection, RS1DLCDisabledSongCollection);
            else
                RefreshDGVAfterSearching(RS1SongCollection, RS1DisabledSongCollection);
        }
        #endregion
        #region Extensions 'n stuff
        private List<RSSongData> GetMatchingSongs(RSJsonDictionary dictionary, string matchingValue)
        {
            List<RSSongData> matchingSongs = new List<RSSongData>();

            foreach (KeyValuePair<string, Dictionary<string, RSSongData>> song in dictionary)
            {
                foreach (KeyValuePair<string, RSSongData> songAttributes in song.Value)
                {
                    if (songAttributes.Value.SongName.ToLower().Contains(tbSearch.Text.ToLower()) || songAttributes.Value.AlbumName.ToLower().Contains(tbSearch.Text.ToLower()) || songAttributes.Value.ArtistName.ToLower().Contains(tbSearch.Text.ToLower()) || songAttributes.Value.Tuning.ToString().TuningToName().ToLower().Contains(tbSearch.Text.ToLower()))
                        matchingSongs.Add(songAttributes.Value);
                }
            }

            return matchingSongs;
        }
        private string TuningJsonToStrings(Dictionary<string, int> tuningJson)
        {
            string tuning = "";
            foreach (KeyValuePair<string, int> stringTuning in tuningJson)
            {
                tuning += stringTuning.Value;
            }
            return tuning;
        }
        private void RefreshDGVAfterSearching(RSJsonDictionary songCollection, RSJsonDictionary FullDisabledSongCollection)
        {
            var filteredSongCollection = GetMatchingSongs(songCollection, tbSearch.Text);
            dgvSongs.Rows.Clear();
            foreach (var song in filteredSongCollection)
                dgvSongs.Rows.Add(false, "Yes", song.SongName, song.ArtistName, song.AlbumName, TuningJsonToStrings(song.Tuning).TuningToName(), song.SongKey);

            filteredSongCollection = GetMatchingSongs(FullDisabledSongCollection, tbSearch.Text);

            foreach (var song in filteredSongCollection)
                dgvSongs.Rows.Add(false, "No", song.SongName, song.ArtistName, song.AlbumName, TuningJsonToStrings(song.Tuning).TuningToName(), song.SongKey);
        }
        private bool DictionaryContains(RSJsonDictionary dictionary, RSSongData songData)
        {
            foreach (KeyValuePair<string, Dictionary<string, RSSongData>> song in dictionary)
            {
                foreach (KeyValuePair<string, RSSongData> songAttributes in song.Value)
                {
                    if (songAttributes.Value.SongName == songData.SongName && songAttributes.Value.AlbumName == songData.AlbumName)
                        return true;
                }
            }
            return false;
        }

        private SongData AttributesToSongData(RSSongData songAttributes, bool enabled = true)
        {
            SongData song = new SongData();
            string tuning = "";

            song.Name = songAttributes.SongName;
            song.Artist = songAttributes.ArtistName;
            song.Album = songAttributes.AlbumName;
            song.SongKey = songAttributes.SongKey;
            song.Enabled = enabled ? "Yes" : "No";
            if (songAttributes.ArrangementName != "Vocals")
            {
                foreach (KeyValuePair<string, int> stringTuning in songAttributes.Tuning)
                {
                    tuning += stringTuning.Value;
                }
                song.Tuning = tuning.TuningToName();
            }
            else
            {
                song.Tuning = "Other";
            }

            return song;
        }

        private void AddToDGV(RSJsonDictionary songCollection, bool enabled = true)
        {
            foreach (KeyValuePair<string, Dictionary<string, RSSongData>> song in songCollection)
            {
                foreach (KeyValuePair<string, RSSongData> songAttributes in song.Value)
                {
                    RSSongData songData = (RSSongData)songAttributes.Value;
                    SongData sngData = AttributesToSongData(songData, enabled);

                    dgvSongs.Rows.Add(false, sngData.Enabled, sngData.Name, sngData.Artist, sngData.Album, sngData.Tuning, sngData.SongKey);
                }
            }
        }
        #endregion

        private void btnSetlists_Click(object sender, EventArgs e)
        {
            SetlistManager setlistMgr = new SetlistManager();
            setlistMgr.Show();
        }
    }
}
