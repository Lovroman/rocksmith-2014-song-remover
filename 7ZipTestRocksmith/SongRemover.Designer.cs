﻿namespace _7ZipTestRocksmith
{
    partial class SongRemover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SongRemover));
            this.btnLoadSongs = new System.Windows.Forms.Button();
            this.dgvSongs = new System.Windows.Forms.DataGridView();
            this.colSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colEnabled = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSongName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colArtist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAlbum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTuning = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSongKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDisableSong = new System.Windows.Forms.Button();
            this.lblRSPath = new System.Windows.Forms.Label();
            this.tbRSPath = new System.Windows.Forms.TextBox();
            this.btnSaveSongs = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.comboGameChoice = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnEnableSong = new System.Windows.Forms.Button();
            this.btnZipForm = new System.Windows.Forms.Button();
            this.btnRestoreBackup = new System.Windows.Forms.Button();
            this.chkRemoveDirs = new System.Windows.Forms.CheckBox();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.btnSetlists = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSongs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadSongs
            // 
            this.btnLoadSongs.Location = new System.Drawing.Point(309, 12);
            this.btnLoadSongs.Name = "btnLoadSongs";
            this.btnLoadSongs.Size = new System.Drawing.Size(75, 23);
            this.btnLoadSongs.TabIndex = 0;
            this.btnLoadSongs.Text = "Load songs";
            this.btnLoadSongs.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // dgvSongs
            // 
            this.dgvSongs.AllowUserToAddRows = false;
            this.dgvSongs.AllowUserToDeleteRows = false;
            this.dgvSongs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSongs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSelect,
            this.colEnabled,
            this.colSongName,
            this.colArtist,
            this.colAlbum,
            this.colTuning,
            this.colSongKey});
            this.dgvSongs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dgvSongs.Location = new System.Drawing.Point(15, 92);
            this.dgvSongs.Name = "dgvSongs";
            this.dgvSongs.Size = new System.Drawing.Size(739, 506);
            this.dgvSongs.TabIndex = 1;
            // 
            // colSelect
            // 
            this.colSelect.FalseValue = "false";
            this.colSelect.HeaderText = "Select";
            this.colSelect.Name = "colSelect";
            this.colSelect.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colSelect.TrueValue = "true";
            // 
            // colEnabled
            // 
            this.colEnabled.HeaderText = "Enabled";
            this.colEnabled.Name = "colEnabled";
            // 
            // colSongName
            // 
            this.colSongName.HeaderText = "Song";
            this.colSongName.Name = "colSongName";
            // 
            // colArtist
            // 
            this.colArtist.HeaderText = "Artist";
            this.colArtist.Name = "colArtist";
            // 
            // colAlbum
            // 
            this.colAlbum.HeaderText = "Album";
            this.colAlbum.Name = "colAlbum";
            // 
            // colTuning
            // 
            this.colTuning.HeaderText = "Tuning";
            this.colTuning.Name = "colTuning";
            // 
            // colSongKey
            // 
            this.colSongKey.HeaderText = "Song Key";
            this.colSongKey.Name = "colSongKey";
            // 
            // btnDisableSong
            // 
            this.btnDisableSong.Location = new System.Drawing.Point(403, 12);
            this.btnDisableSong.Name = "btnDisableSong";
            this.btnDisableSong.Size = new System.Drawing.Size(87, 23);
            this.btnDisableSong.TabIndex = 4;
            this.btnDisableSong.Text = "Disable song";
            this.btnDisableSong.UseVisualStyleBackColor = true;
            this.btnDisableSong.Click += new System.EventHandler(this.btnDeleteSong_Click);
            // 
            // lblRSPath
            // 
            this.lblRSPath.AutoSize = true;
            this.lblRSPath.Location = new System.Drawing.Point(81, 15);
            this.lblRSPath.Name = "lblRSPath";
            this.lblRSPath.Size = new System.Drawing.Size(84, 13);
            this.lblRSPath.TabIndex = 6;
            this.lblRSPath.Text = "Rocksmith path:";
            // 
            // tbRSPath
            // 
            this.tbRSPath.Location = new System.Drawing.Point(181, 12);
            this.tbRSPath.Name = "tbRSPath";
            this.tbRSPath.Size = new System.Drawing.Size(100, 20);
            this.tbRSPath.TabIndex = 5;
            // 
            // btnSaveSongs
            // 
            this.btnSaveSongs.Location = new System.Drawing.Point(601, 11);
            this.btnSaveSongs.Name = "btnSaveSongs";
            this.btnSaveSongs.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSongs.TabIndex = 7;
            this.btnSaveSongs.Text = "Save songs";
            this.btnSaveSongs.UseVisualStyleBackColor = true;
            this.btnSaveSongs.Click += new System.EventHandler(this.btnSaveSongs_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(121, 43);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(383, 20);
            this.tbSearch.TabIndex = 8;
            this.tbSearch.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Search:";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(524, 40);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(128, 23);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // comboGameChoice
            // 
            this.comboGameChoice.FormattingEnabled = true;
            this.comboGameChoice.Items.AddRange(new object[] {
            "Rocksmith 1",
            "Rocksmith 1 DLC",
            "Rocksmith 2014"});
            this.comboGameChoice.Location = new System.Drawing.Point(319, 65);
            this.comboGameChoice.Name = "comboGameChoice";
            this.comboGameChoice.Size = new System.Drawing.Size(121, 21);
            this.comboGameChoice.TabIndex = 11;
            this.comboGameChoice.SelectedIndexChanged += new System.EventHandler(this.comboGameChoice_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Songs from:";
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(72, 65);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(93, 23);
            this.btnSelectAll.TabIndex = 13;
            this.btnSelectAll.Text = "Select all/none";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnEnableSong
            // 
            this.btnEnableSong.Location = new System.Drawing.Point(511, 11);
            this.btnEnableSong.Name = "btnEnableSong";
            this.btnEnableSong.Size = new System.Drawing.Size(75, 23);
            this.btnEnableSong.TabIndex = 14;
            this.btnEnableSong.Text = "Enable song";
            this.btnEnableSong.UseVisualStyleBackColor = true;
            this.btnEnableSong.Click += new System.EventHandler(this.btnEnableSong_Click);
            // 
            // btnZipForm
            // 
            this.btnZipForm.Location = new System.Drawing.Point(12, 9);
            this.btnZipForm.Name = "btnZipForm";
            this.btnZipForm.Size = new System.Drawing.Size(34, 23);
            this.btnZipForm.TabIndex = 15;
            this.btnZipForm.Text = "ZIP form- don\'t use";
            this.btnZipForm.UseVisualStyleBackColor = true;
            this.btnZipForm.Click += new System.EventHandler(this.btnZipForm_Click);
            // 
            // btnRestoreBackup
            // 
            this.btnRestoreBackup.Location = new System.Drawing.Point(658, 40);
            this.btnRestoreBackup.Name = "btnRestoreBackup";
            this.btnRestoreBackup.Size = new System.Drawing.Size(109, 23);
            this.btnRestoreBackup.TabIndex = 16;
            this.btnRestoreBackup.Text = "Restore backup";
            this.btnRestoreBackup.UseVisualStyleBackColor = true;
            this.btnRestoreBackup.Click += new System.EventHandler(this.btnRestoreBackup_Click);
            // 
            // chkRemoveDirs
            // 
            this.chkRemoveDirs.AutoSize = true;
            this.chkRemoveDirs.Location = new System.Drawing.Point(464, 69);
            this.chkRemoveDirs.Name = "chkRemoveDirs";
            this.chkRemoveDirs.Size = new System.Drawing.Size(153, 17);
            this.chkRemoveDirs.TabIndex = 17;
            this.chkRemoveDirs.Text = "Remove dirs when finished";
            this.chkRemoveDirs.UseVisualStyleBackColor = true;
            // 
            // btnSetlists
            // 
            this.btnSetlists.Location = new System.Drawing.Point(626, 65);
            this.btnSetlists.Name = "btnSetlists";
            this.btnSetlists.Size = new System.Drawing.Size(128, 23);
            this.btnSetlists.TabIndex = 18;
            this.btnSetlists.Text = "Open setlist manager";
            this.btnSetlists.UseVisualStyleBackColor = true;
            this.btnSetlists.Click += new System.EventHandler(this.btnSetlists_Click);
            // 
            // SongRemover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 624);
            this.Controls.Add(this.btnSetlists);
            this.Controls.Add(this.chkRemoveDirs);
            this.Controls.Add(this.btnRestoreBackup);
            this.Controls.Add(this.btnZipForm);
            this.Controls.Add(this.btnEnableSong);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboGameChoice);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.btnSaveSongs);
            this.Controls.Add(this.lblRSPath);
            this.Controls.Add(this.tbRSPath);
            this.Controls.Add(this.btnDisableSong);
            this.Controls.Add(this.dgvSongs);
            this.Controls.Add(this.btnLoadSongs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SongRemover";
            this.Text = "Rocksmith 2014 Song Remover";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SongRemover_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSongs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadSongs;
        private System.Windows.Forms.DataGridView dgvSongs;
        private System.Windows.Forms.Button btnDisableSong;
        private System.Windows.Forms.Label lblRSPath;
        private System.Windows.Forms.Button btnSaveSongs;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox comboGameChoice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEnabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSongName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colArtist;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAlbum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTuning;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSongKey;
        private System.Windows.Forms.Button btnEnableSong;
        private System.Windows.Forms.Button btnZipForm;
        private System.Windows.Forms.Button btnRestoreBackup;
        private System.Windows.Forms.CheckBox chkRemoveDirs;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.Button btnSetlists;
        public System.Windows.Forms.TextBox tbRSPath;
    }
}

