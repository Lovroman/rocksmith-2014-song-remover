﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using _7ZipTestRocksmith.Utilities;

namespace _7ZipTestRocksmith
{
    public partial class SetlistManager : Form
    {
        public SetlistManager()
        {
            InitializeComponent();
        }

        string PZipPath = "7za.exe";
        ProcessStartInfo pZip = new ProcessStartInfo();

        private void btnSaveCurrentSongFilesState_Click(object sender, EventArgs e)
        {
            SongRemover sngRemover = new SongRemover();
            pZip.FileName = PZipPath;

            if (tbFileName.Text != "")
            {
                if (sngRemover.tbRSPathText != "" && Directory.Exists(sngRemover.tbRSPathText))
                {
                    string dlcFolderPath = Path.Combine(sngRemover.tbRSPath.Text, "dlc");
                    pZip.Arguments = "a " + Path.Combine(dlcFolderPath, tbFileName.Text, ".zip") + "\"" + Path.Combine(dlcFolderPath, "rs1compatibilitydlc_p.psarc") + "\" \"" + Path.Combine(dlcFolderPath, "rs1compatibilitydisc_p.psarc") + "\" \"" + Path.Combine(dlcFolderPath, "cache.psarc\"");
                }
                else
                    pZip.Arguments = "a " + tbFileName.Text + ".zip " + "\"rs1compatibilitydlc_p.psarc\" " + "\"rs1compatibilitydisc_p.psarc\" " + "\"cache.psarc\"";

                Process x = Process.Start(pZip);
                x.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                x.WaitForExit();

                listSavedSongFiles.Items.Add(tbFileName.Text);
            }
            else
                MessageBox.Show("Please fill file name textbox!", "File name empty");
        }

        private void btnLoadSelectedSongFile_Click(object sender, EventArgs e)
        {
            if (listSavedSongFiles.SelectedItems.Count > 0)
            {
                SongRemover sngRemover = new SongRemover();
                pZip.FileName = PZipPath;

                //pZip.RedirectStandardOutput = true;
                //pZip.UseShellExecute = false;

                if (!Directory.Exists("ExtractedSongFiles"))
                    Directory.CreateDirectory("ExtractedSongFiles");

                pZip.Arguments = "e " + listSavedSongFiles.SelectedItem.ToString() + " -oExtractedSongFiles -aoa";

                Process x = Process.Start(pZip);
                x.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                x.WaitForExit();

                if (sngRemover.tbRSPathText != "" && Directory.Exists(sngRemover.tbRSPathText))
                {
                    string dlcFolderPath = Path.Combine(sngRemover.tbRSPath.Text, "dlc");

                    try
                    {
                        if (File.Exists("ExtractedSongFiles\\rs1compatibilitydlc_p.psarc"))
                            File.Copy("ExtractedSongFiles\\rs1compatibilitydlc_p.psarc", Path.Combine(dlcFolderPath, "rs1compatibilitydlc_p.psarc"), true);
                        if (File.Exists("ExtractedSongFiles\\rs1compatibilitydisc_p.psarc"))
                            File.Copy("ExtractedSongFiles\\rs1compatibilitydisc_p.psarc", Path.Combine(dlcFolderPath, "rs1compatibilitydisc_p.psarc"), true);
                        if (File.Exists("ExtractedSongFiles\\cache.psarc"))
                            File.Copy("ExtractedSongFiles\\cache.psarc", Path.Combine(sngRemover.tbRSPath.Text, "cache.psarc"), true);
                    }catch(UnauthorizedAccessException)
                    { }
                }
                MessageBox.Show("Song file(s) loaded!");
            }
        }

        private void btnDeleteSelectedSongFile_Click(object sender, EventArgs e)
        {
            SongRemover sngRemover = new SongRemover();
            try
            {
                if (listSavedSongFiles.SelectedItems.Count > 0)
                {
                    string songArchivePath = listSavedSongFiles.SelectedItem.ToString();
                    if (sngRemover.tbRSPath.Text != "")
                        songArchivePath = Path.Combine(sngRemover.tbRSPath.Text, "dlc", songArchivePath);

                    if (File.Exists(songArchivePath))
                    {
                        File.Delete(songArchivePath);
                    }
                    listSavedSongFiles.Items.Remove(listSavedSongFiles.SelectedItem);
                }
            }
            catch (IOException)
            {
                MessageBox.Show("Unable to delete the song archive!", "IO error");
            }
        }

        private void SetlistManager_Load(object sender, EventArgs e)
        {
            SongRemover sngRemover = new SongRemover();
            IEnumerable<string> files = null;
            ThreadPool.QueueUserWorkItem(delegate
            {
                if (sngRemover.tbRSPath.Text != "" && Directory.Exists(sngRemover.tbRSPathText))
                    files = Directory.EnumerateFiles(Path.Combine(sngRemover.tbRSPath.Text, "dlc"), "*.zip", SearchOption.TopDirectoryOnly).Select(Path.GetFileName);
                else
                    files = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*.zip", SearchOption.TopDirectoryOnly).Select(Path.GetFileName);

                foreach (var songFile in files)
                {
                    listSavedSongFiles.InvokeIfRequired(delegate
                    {
                        listSavedSongFiles.Items.Add(songFile);
                    });
                }
            });
        }

        private void btnSetlistToBatchFile_Click(object sender, EventArgs e)
        {
            if (listSavedSongFiles.SelectedItems.Count > 0)
            {
                SongRemover sngRemover = new SongRemover();
                StringBuilder sb = new StringBuilder();

                string batchFilePath = Path.GetFileNameWithoutExtension(listSavedSongFiles.SelectedItem.ToString()) + ".bat";
                string echoOffCommand = "@echo off";
                string zipCommand = "7za e " + listSavedSongFiles.SelectedItem.ToString() + " -odlc -aoa";
                string startRocksmithCommand = @"start /d """" steam://rungameid/221680";
                string pauseCommand = "pause";

                sb.AppendLine(echoOffCommand);
                sb.AppendLine(zipCommand);
                sb.AppendLine(startRocksmithCommand);
                sb.AppendLine(pauseCommand);

                File.WriteAllText(batchFilePath, sb.ToString());

                if (sngRemover.tbRSPathText != "" && Directory.Exists(sngRemover.tbRSPathText))
                {
                    if (!File.Exists(batchFilePath))
                        File.Copy("7za.exe", sngRemover.tbRSPathText);
                    File.Move(batchFilePath, Path.Combine(sngRemover.tbRSPathText, batchFilePath));
                }
            }
        }
    }
}
