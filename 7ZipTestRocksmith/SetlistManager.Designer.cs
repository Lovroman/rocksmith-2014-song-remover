﻿namespace _7ZipTestRocksmith
{
    partial class SetlistManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetlistManager));
            this.listSavedSongFiles = new System.Windows.Forms.ListBox();
            this.btnSaveCurrentSongFilesState = new System.Windows.Forms.Button();
            this.btnLoadSelectedSongFile = new System.Windows.Forms.Button();
            this.btnDeleteSelectedSongFile = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.btnSetlistToBatchFile = new System.Windows.Forms.Button();
            this.treeListView1 = new BrightIdeasSoftware.TreeListView();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // listSavedSongFiles
            // 
            this.listSavedSongFiles.FormattingEnabled = true;
            this.listSavedSongFiles.Location = new System.Drawing.Point(12, 12);
            this.listSavedSongFiles.Name = "listSavedSongFiles";
            this.listSavedSongFiles.Size = new System.Drawing.Size(94, 30);
            this.listSavedSongFiles.TabIndex = 0;
            // 
            // btnSaveCurrentSongFilesState
            // 
            this.btnSaveCurrentSongFilesState.Location = new System.Drawing.Point(12, 295);
            this.btnSaveCurrentSongFilesState.Name = "btnSaveCurrentSongFilesState";
            this.btnSaveCurrentSongFilesState.Size = new System.Drawing.Size(113, 36);
            this.btnSaveCurrentSongFilesState.TabIndex = 1;
            this.btnSaveCurrentSongFilesState.Text = "Save current song file state";
            this.btnSaveCurrentSongFilesState.UseVisualStyleBackColor = true;
            this.btnSaveCurrentSongFilesState.Click += new System.EventHandler(this.btnSaveCurrentSongFilesState_Click);
            // 
            // btnLoadSelectedSongFile
            // 
            this.btnLoadSelectedSongFile.Location = new System.Drawing.Point(145, 295);
            this.btnLoadSelectedSongFile.Name = "btnLoadSelectedSongFile";
            this.btnLoadSelectedSongFile.Size = new System.Drawing.Size(113, 36);
            this.btnLoadSelectedSongFile.TabIndex = 2;
            this.btnLoadSelectedSongFile.Text = "Load selected song file";
            this.btnLoadSelectedSongFile.UseVisualStyleBackColor = true;
            this.btnLoadSelectedSongFile.Click += new System.EventHandler(this.btnLoadSelectedSongFile_Click);
            // 
            // btnDeleteSelectedSongFile
            // 
            this.btnDeleteSelectedSongFile.Location = new System.Drawing.Point(280, 295);
            this.btnDeleteSelectedSongFile.Name = "btnDeleteSelectedSongFile";
            this.btnDeleteSelectedSongFile.Size = new System.Drawing.Size(113, 36);
            this.btnDeleteSelectedSongFile.TabIndex = 3;
            this.btnDeleteSelectedSongFile.Text = "Delete selected song file";
            this.btnDeleteSelectedSongFile.UseVisualStyleBackColor = true;
            this.btnDeleteSelectedSongFile.Click += new System.EventHandler(this.btnDeleteSelectedSongFile_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(145, 269);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(311, 20);
            this.tbFileName.TabIndex = 4;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(84, 272);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(55, 13);
            this.lblFileName.TabIndex = 5;
            this.lblFileName.Text = "File name:";
            // 
            // btnSetlistToBatchFile
            // 
            this.btnSetlistToBatchFile.Location = new System.Drawing.Point(406, 295);
            this.btnSetlistToBatchFile.Name = "btnSetlistToBatchFile";
            this.btnSetlistToBatchFile.Size = new System.Drawing.Size(113, 36);
            this.btnSetlistToBatchFile.TabIndex = 6;
            this.btnSetlistToBatchFile.Text = "Make a batchfile with the setlist";
            this.btnSetlistToBatchFile.UseVisualStyleBackColor = true;
            this.btnSetlistToBatchFile.Click += new System.EventHandler(this.btnSetlistToBatchFile_Click);
            // 
            // treeListView1
            // 
            this.treeListView1.Location = new System.Drawing.Point(12, 43);
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.OwnerDraw = true;
            this.treeListView1.ShowGroups = false;
            this.treeListView1.Size = new System.Drawing.Size(502, 197);
            this.treeListView1.TabIndex = 8;
            this.treeListView1.UseCompatibleStateImageBehavior = false;
            this.treeListView1.View = System.Windows.Forms.View.Details;
            this.treeListView1.VirtualMode = true;
            // 
            // SetlistManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 337);
            this.Controls.Add(this.treeListView1);
            this.Controls.Add(this.btnSetlistToBatchFile);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.btnDeleteSelectedSongFile);
            this.Controls.Add(this.btnLoadSelectedSongFile);
            this.Controls.Add(this.btnSaveCurrentSongFilesState);
            this.Controls.Add(this.listSavedSongFiles);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetlistManager";
            this.Text = "Setlist Manager";
            this.Load += new System.EventHandler(this.SetlistManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listSavedSongFiles;
        private System.Windows.Forms.Button btnSaveCurrentSongFilesState;
        private System.Windows.Forms.Button btnLoadSelectedSongFile;
        private System.Windows.Forms.Button btnDeleteSelectedSongFile;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Button btnSetlistToBatchFile;
        private BrightIdeasSoftware.TreeListView treeListView1;
    }
}